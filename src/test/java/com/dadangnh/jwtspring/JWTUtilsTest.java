package com.dadangnh.jwtspring;

import com.dadangnh.jwtspring.utils.JWTUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class JWTUtilsTest {

    private static final Logger logger = LogManager.getLogger();

    /*
        Create a simple JWT, decode it, and assert the claims
     */
    @Test
    public void createAndDecodeJWT() {

        String jwtId = "IDJWTValid";
        String jwtIssuer = "DadangNH.com";
        String jwtSubject = "Dadang NH";
        int jwtTimeToLive = 800000;

        String jwt = JWTUtils.createJWT(
                jwtId, // claim = jti
                jwtIssuer, // claim = iss
                jwtSubject, // claim = sub
                jwtTimeToLive // used to calculate expiration (claim = exp)
        );
        
        logger.info("jwt = \"" + jwt.toString() + "\"");

        Claims claims = JWTUtils.decodeJWT(jwt);

        logger.info("claims = " + claims.toString());

        assertEquals(jwtId, claims.getId());
        assertEquals(jwtIssuer, claims.getIssuer());
        assertEquals(jwtSubject, claims.getSubject());

    }

    /*
        Attempt to decode a bogus JWT and expect an exception
     */
    @Test()
    public void decodeShouldFail() {

        String notAJwt = "This is not a JWT";

        MalformedJwtException thrown = Assertions
                .assertThrows(MalformedJwtException.class, () -> {
                    JWTUtils.decodeJWT(notAJwt);
                }, "Not a JWT Token");
        Assertions.assertEquals("JWT strings must contain exactly 2 period characters. Found: 0", thrown.getMessage());
    }

    /*
    Create a simple JWT, modify it, and try to decode it
 */
    @Test()
    public void createAndDecodeTamperedJWT() {

        String jwtId = "TESTJWTInvalid";
        String jwtIssuer = "bukan.DadangNH.com";
        String jwtSubject = "BUKAN Dadang NH";
        int jwtTimeToLive = 800000;

        String jwt = JWTUtils.createJWT(
                jwtId,
                jwtIssuer,
                jwtSubject,
                jwtTimeToLive
        );

        logger.info("jwt = \"" + jwt.toString() + "\"");

        StringBuilder tamperedJwt = new StringBuilder(jwt);
        tamperedJwt.setCharAt(22, 'I');

        logger.info("tamperedJwt = \"" + tamperedJwt.toString() + "\"");

        assertNotEquals(jwt, tamperedJwt);

        // this will fail with a SignatureException
        SignatureException thrown = Assertions
                .assertThrows(SignatureException.class, () -> {
                    JWTUtils.decodeJWT(tamperedJwt.toString());
                }, "JWT Signature is tampered");
        Assertions.assertEquals("JWT signature does not match locally computed signature. JWT validity cannot be asserted and should not be trusted.", thrown.getMessage());
    }

}
